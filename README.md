# mqa-ui

A single page application that represents the MQA validation results.

## Setup

* Install all of the following software        
  * NodeJS
  * Git >= 2.17
* Clone the directory and enter it
    
        git clone git@gitlab.com:european-data-portal/mqa/mqa-ui.git

Create a `conf/dev.env.js` file and adjust the content as needed. The `ROOT_API` refers to the metrics-service API. 

```
'use strict'

const merge = require('webpack-merge')
module.exports = merge(devEnv, {

  NODE_ENV: '"development"',
  ROOT_API: '"URL"'
})
```

## Build and run the project

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

```

mqa-ui is a vue.js applicaiton. For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


## Run it via Docker


* Start your docker daemon
* Build the application as described in Windows or Linux
* Adjust the port number (EXPOSE in the Dockerfile)
* Build the image: docker build -t edp/mqa-ui .

Run the image, adjusting the port number as set in step iii: docker run -i -p 8095:8095 edp/mqa-mqa

Configuration can be changed without rebuilding the image by overriding variables: -e PORT=8096
